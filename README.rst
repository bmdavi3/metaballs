Metaballs
==========================================================

Project Description:
--------------------

Who doesn't love `metaballs <https://bmdavi3.gitlab.io/metaballs/>`_?

I had gotten metaballs working a while back using C and OpenGL, and I was real proud of myself.  Years passed, and I somehow lost the source code (pre-GitHub / GitLab).  So I gave it another go in JavaScript and WebGL.  I even got to write a shader this time, hooray!


Basic gist of how metaballs work:
---------------------------------

We create a three dimensional grid of a certain size, say 120x120x120, and put two special points into it.  These two points emit 'energy' that every other point in the grid is going to receive, according to a formula we define.  The closer, the more energy received.  Points that receive a sum of energy past a threshold we define, will be 'inside' the shape, and those with less than the cutoff, will be outside.

Once we have inside / outside determined for each point in our 120x120x120 grid, we can display each of the inside points as little circles, or points, with OpenGL and they'll look like two spheres (and if we define our energy function just right, blobs!).  Drawing each inside point this way will work, but it's going to take a lot of points to create a smooth shape.  So instead we don't look at one individual point and draw that, we look at cubes of points that straddle the inside / outside boundary, and use a lookup table to draw a triangle instead.  The `marching cubes <https://en.wikipedia.org/wiki/Marching_cubes>`_ algorithm will help us draw these triangles, and let us use a much coarser grid (fewer energy computations) to get a nice looking image.

Marching cubes lets us calculate fewer energy values since our grid will be coarser, but we can still do better.  Since we only end up drawing triangles near points on the inside / outside boundary of our shape(s), we can, for each of our two energy points, start at that point and head straight in any direction, calculating energies until we hit an outside point.  Once we do, we know we are on a boundary, and we can more or less "put our hand on the wall" and recursively follow these points around the shape, examining any neighbor point that isn't totally inside, or totally outside.  That way, we aren't calculating any more energy values than we need to.  When those are calculated, draw our triangles as usual and we're done!
